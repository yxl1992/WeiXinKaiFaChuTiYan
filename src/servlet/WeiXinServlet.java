package servlet;



import org.dom4j.DocumentException;
import po.TextMessage;
import untils.MessageUtil;
import untils.SignUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

/**
 * @Author SPF
 * @Date 2017/6/6
 */
public class WeiXinServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp，nonce参数
        String signature = request.getParameter("signature");
        //时间戳
        String timestamp = request.getParameter("timestamp");
        //随机数
        String nonce = request.getParameter("nonce");
        //随机字符串
        String echostr = request.getParameter("echostr");

        if (SignUtil.checkSignature(signature, timestamp, nonce)) {
            System.out.println("[signature: " + signature + "]<-->[timestamp: " + timestamp + "]<-->[nonce: " + nonce + "]<-->[echostr: " + echostr + "]");
            response.getOutputStream().println(echostr);
        }
    }

    /**
     * 消息的接受与响应
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       req.setCharacterEncoding("UTF-8");
       resp.setCharacterEncoding("UTF-8");
        PrintWriter out=resp.getWriter();

        try {
            Map<String ,String> map= MessageUtil.xmltoMap(req);
            String  fromUserName=map.get("FromUserName");
            String  toUserName=map.get("ToUserName");
           // String  createTime=map.get("CreateTime");
            String  msgType=map.get("MsgType");
            String  content=map.get("Content");
           // String  msgId=map.get("MsgId");
                String msg=null;
            if(MessageUtil.MESSAGE_TEXT.equals(msgType)){
                if("1".equals(content)){
                    msg=MessageUtil.inintText(toUserName,fromUserName,MessageUtil.firstMenu());
                }else  if("2".equals(content)){
                    msg=MessageUtil.inintText(toUserName,fromUserName,MessageUtil.secondMenu());
                }else  if("？".equals(content)||"?".equals(content)){
                    msg=MessageUtil.inintText(toUserName,fromUserName,MessageUtil.menuText());
              }else  if("3".equals(content)){
                    msg=MessageUtil.initNewsMessage(toUserName,fromUserName);
                } else{TextMessage textMessage=new TextMessage();
                    textMessage.setFromUserName(toUserName);
                    textMessage.setToUserName(fromUserName);
                    textMessage.setMsgType("text");
                    textMessage.setCreateTime( new Date().getTime());
                    textMessage.setContent("您发送的消息是："+content);
                    msg=MessageUtil.textMessageToXml(textMessage);}


            }else if(MessageUtil.MESSAGE_EVENT.equals(msgType)){
                String   eventType= map.get("Event");
                if(MessageUtil.MESSAGE_SUBSCRIBE.equals(eventType)){
                    msg=MessageUtil.inintText(toUserName,fromUserName,MessageUtil.menuText());

                }

            }
            System.out.println(msg);
            out.println(msg);
        } catch (DocumentException e) {
            e.printStackTrace();
        }finally {

            out.close();
        }
    }
}
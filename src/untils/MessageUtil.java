package untils;

import com.thoughtworks.xstream.XStream;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import po.News;
import po.NewsMessage;
import po.TextMessage;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class MessageUtil {
    public static final String MESSAGE_TEXT="text";
    public static final String MESSAGE_NEWS="news";
    public static final String MESSAGE_IMAGE="image";
    public static final String MESSAGE_VOICE="voice";
    public static final String MESSAGE_VIDEO="video";
    public static final String MESSAGE_LINK="link";
    public static final String MESSAGE_LOCATION="location";
    public static final String MESSAGE_EVENT="event";
    public static final String MESSAGE_SUBSCRIBE="subscribe";
    public static final String MESSAGE_UNSUBSCRIBE="unsubscribe";
    public static final String MESSAGE_CLICK="CLICK";
    public static final String MESSAGE_VIEW="VIEW";

    public  static Map<String ,String> xmltoMap(HttpServletRequest httpServletRequest) throws IOException, DocumentException {
        //Xml转map
            Map<String,String> map=new HashMap<String,String>();
        SAXReader reader=new SAXReader();

        InputStream inputStream= httpServletRequest.getInputStream();
        Document document=reader.read(inputStream);
        Element root =document.getRootElement();
        List<Element>list =root.elements();
        for(Element e:list){
            map.put(e.getName(),e.getText());
        }
        inputStream.close();
        return map;
    }
    //将文本消息对象转换为Xml
    public  static String textMessageToXml(TextMessage textMessage) {
        XStream xStream = new XStream();
        xStream.alias("xml",textMessage.getClass());
        return xStream.toXML(textMessage);
    }

    /**
     * 关注后主菜单
     * @return
     */
    public static  String menuText(){
        StringBuffer sb=new StringBuffer();
        sb.append("欢迎您的关注，请您按照菜单提示进行操作：\n\n");
        sb.append("1.Who  am  I? \n");
        sb.append("2.你想找什么？\n");
        sb.append("回复？可以调出此菜单。");
        return  sb.toString();
    }
    /**
     * 文本消息的回复
     */
    public static  String  inintText(String toUserName,String fromUserName,String content){
        TextMessage textMessage=new TextMessage();
        textMessage.setFromUserName(toUserName);
        textMessage.setToUserName(fromUserName);
        textMessage.setMsgType(MessageUtil.MESSAGE_TEXT);
        textMessage.setCreateTime( new Date().getTime());
        textMessage.setContent(content);
       return MessageUtil.textMessageToXml(textMessage);
    }
    public  static  String  firstMenu(){
        StringBuffer sb=new StringBuffer();
        sb.append("我是包子，我的微信号是yangxule123,这是我的个人订阅号，我会不定期地推送一些好玩的东西给大家，希望大家能提出宝贵的建议，让我们一块愉快地玩耍吧！");

        return  sb.toString();
    }
    public  static  String secondMenu(){
        StringBuffer sb=new StringBuffer();
        sb.append("肯定不是找我的！那你想找什么？告诉我，我会推送给你的！");

        return  sb.toString();
    }

    /**
     * 图文消息转换为XML
     * @param newsMessage
     * @return
     */
    public  static String newsMessageToXml(NewsMessage newsMessage) {
        XStream xStream = new XStream();
        xStream.alias("xml",newsMessage.getClass());
        xStream.alias("item",new News().getClass());
        return xStream.toXML(newsMessage);
    }

    /**
     * 图文消息的组装
     * @param toUserName
     * @param fromUserName
     * @param
     * @return
     */
    public  static  String  initNewsMessage(String toUserName,String fromUserName){
        String message=null;
        List<News> list=new ArrayList<News>();
        NewsMessage newsMessage=new NewsMessage();
        News news =new News();
        news.setTitle("Who am I?");
        news.setDescription("我是包子，我的微信号是yangxule123,这是我的个人订阅号，我会不定期地推送一些好玩的东西给大家，希望大家能提出宝贵的建议，让我们一块愉快地玩耍吧！");
        news.setPicUrl("http://dfvn8g.natappfree.cc/image/timg.jpg");
        news.setUrl("www.feizl.com/html/66010.htm");
        News news1 =new News();
        news1.setTitle("Who am I?");
        news1.setDescription("我是包子，我的微信号是yangxule123,这是我的个人订阅号，我会不定期地推送一些好玩的东西给大家，希望大家能提出宝贵的建议，让我们一块愉快地玩耍吧！");
        news1.setPicUrl("http://dfvn8g.natappfree.cc/image/timg.jpg");
        news1.setUrl("www.feizl.com/html/66010.htm");
        list.add(news);
        list.add(news1);

        newsMessage.setToUserName(fromUserName);
        newsMessage.setFromUserName(toUserName);
        newsMessage.setCreateTime( new Date().getTime());
        newsMessage.setMsgType(MessageUtil.MESSAGE_NEWS);
        newsMessage.setArticleCount(list.size());
        newsMessage.setArticles(list);
        message=newsMessageToXml(newsMessage);
        return  message;
    }

}
